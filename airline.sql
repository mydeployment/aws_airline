-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: airline
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bookings` (
  `bookingNo` int NOT NULL AUTO_INCREMENT,
  `bookingStatus` varchar(255) DEFAULT NULL,
  `paymentStatus` varchar(255) DEFAULT NULL,
  `seatNo` int NOT NULL,
  `totalAmount` double NOT NULL,
  `passengerId` int DEFAULT NULL,
  `scheduleId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `priceId` int DEFAULT NULL,
  PRIMARY KEY (`bookingNo`),
  KEY `FKn0268hu822335fgbb8bvqdu44` (`passengerId`),
  KEY `FKn2kgytygw8l1m6jqhksijbqk` (`scheduleId`),
  KEY `FKmukqr70djt67i1i3kkfpxroo3` (`userId`),
  KEY `FKoww65hbfsf5rhoto247i6ixcn` (`priceId`),
  CONSTRAINT `FKmukqr70djt67i1i3kkfpxroo3` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`),
  CONSTRAINT `FKn0268hu822335fgbb8bvqdu44` FOREIGN KEY (`passengerId`) REFERENCES `passenger` (`passengerId`),
  CONSTRAINT `FKn2kgytygw8l1m6jqhksijbqk` FOREIGN KEY (`scheduleId`) REFERENCES `scheduleflight` (`scheduleId`),
  CONSTRAINT `FKoww65hbfsf5rhoto247i6ixcn` FOREIGN KEY (`priceId`) REFERENCES `price` (`priceId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` VALUES (1,'Booked','Pay',0,0,8,2,6,NULL),(2,'Booked','Pay',0,0,8,2,6,NULL),(4,'Booked','Pay',0,0,8,2,6,NULL),(5,'Booked','Pay',0,0,8,2,6,NULL),(6,'Booked','Pay',0,0,10,9,7,NULL),(7,'cancelled','Pay',0,0,11,9,7,NULL),(8,'Booked','Pay',0,0,11,9,7,NULL),(9,'cancelled','Pay',0,0,12,9,7,NULL),(10,'Booked','Pay',0,0,12,9,7,NULL),(11,'Booked','Pay',0,0,13,9,7,NULL),(12,'Booked','Pay',0,0,14,9,7,NULL),(13,'Booked','Pay',0,0,14,9,7,NULL),(14,'Booked','Pay',0,0,14,9,7,NULL),(15,'Booked','Pay',0,0,14,9,7,NULL),(16,'Booked','Pay',0,0,14,9,7,NULL),(17,'Booked','Pay',0,0,14,9,7,NULL),(18,'Booked','Pay',0,0,14,9,7,NULL),(19,'Booked','Pay',0,0,8,2,6,NULL),(20,'Booked','Pay',0,0,16,9,7,NULL),(21,'Booked','Pay',0,0,17,9,7,NULL);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback` (
  `feedback_id` int NOT NULL AUTO_INCREMENT,
  `comment` varchar(500) DEFAULT NULL,
  `overall_experience` varchar(20) DEFAULT NULL,
  `service_quality` varchar(20) DEFAULT NULL,
  `bookingNo` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  PRIMARY KEY (`feedback_id`),
  KEY `FKjv03psluhoa2agdxktc1lygxp` (`bookingNo`),
  KEY `FKn81f039k72r0abjpcac6u9kvo` (`userId`),
  CONSTRAINT `FKjv03psluhoa2agdxktc1lygxp` FOREIGN KEY (`bookingNo`) REFERENCES `bookings` (`bookingNo`),
  CONSTRAINT `FKn81f039k72r0abjpcac6u9kvo` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback_tb`
--

DROP TABLE IF EXISTS `feedback_tb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feedback_tb` (
  `feedBack_id` int NOT NULL AUTO_INCREMENT,
  `emailId` varchar(15) DEFAULT NULL,
  `firstName` varchar(15) DEFAULT NULL,
  `lastName` varchar(15) DEFAULT NULL,
  `message` varchar(150) DEFAULT NULL,
  `title` varchar(15) DEFAULT NULL,
  `userId` int DEFAULT NULL,
  PRIMARY KEY (`feedBack_id`),
  KEY `FKqyrm3t8abbnl3paqs0ki2dm9u` (`userId`),
  CONSTRAINT `FKqyrm3t8abbnl3paqs0ki2dm9u` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback_tb`
--

LOCK TABLES `feedback_tb` WRITE;
/*!40000 ALTER TABLE `feedback_tb` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback_tb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight`
--

DROP TABLE IF EXISTS `flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flight` (
  `flightId` int NOT NULL AUTO_INCREMENT,
  `airlineName` varchar(255) DEFAULT NULL,
  `buisnessClassSeats` int NOT NULL,
  `cabinBaggageCapacity` int NOT NULL,
  `checkinBaggageCapacity` int NOT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `economyClassSeats` int NOT NULL,
  `firstClassSeats` int NOT NULL,
  PRIMARY KEY (`flightId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight`
--

LOCK TABLES `flight` WRITE;
/*!40000 ALTER TABLE `flight` DISABLE KEYS */;
INSERT INTO `flight` VALUES (1,'air-india',2,30,20,'TATA',4,3),(5,'Airasia',20,30,30,'Boing',20,20),(6,'AirIndia',20,20,20,'TATA',20,20);
/*!40000 ALTER TABLE `flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offer`
--

DROP TABLE IF EXISTS `offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `offer` (
  `offer_id` int NOT NULL AUTO_INCREMENT,
  `offer_name` varchar(255) DEFAULT NULL,
  `offer_price` double DEFAULT NULL,
  PRIMARY KEY (`offer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offer`
--

LOCK TABLES `offer` WRITE;
/*!40000 ALTER TABLE `offer` DISABLE KEYS */;
INSERT INTO `offer` VALUES (3,'diwali',2556),(4,'dasra',200);
/*!40000 ALTER TABLE `offer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passenger`
--

DROP TABLE IF EXISTS `passenger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `passenger` (
  `passengerId` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `passengerAadharNo` int NOT NULL,
  `passengerDob` date DEFAULT NULL,
  `passengerName` varchar(255) DEFAULT NULL,
  `passengerpassportNo` int NOT NULL,
  `phoneNo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`passengerId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passenger`
--

LOCK TABLES `passenger` WRITE;
/*!40000 ALTER TABLE `passenger` DISABLE KEYS */;
INSERT INTO `passenger` VALUES (1,'kk@gmail.com',5566,'2021-01-06','tejo',9876,'54757868'),(2,'kk@gmail.com',5566,'2022-07-19','komal',9876,'54757868'),(3,'papmdpgwap123@gmail.com',1364646,'2021-01-19','pm',0,'8888111923'),(4,'papmdpgwap123@gmail.com',123456789,'2012-05-07','payal',0,'8888111923'),(5,'papmdpgwap123@gmail.com',345676976,'2022-09-11','Tejo',0,'8888111923'),(6,'papmdpgwap123@gmail.com',472578,'2022-09-05','om',0,'8888111923'),(8,'abc@gmail.com',46466546,'2018-02-08','khushal khairnar',0,'8888111923'),(9,'kk@gmail.com',1111,'1997-04-05','payal',2233,'54757868'),(10,'papmdpgwap123@gmail.com',87875454,'2022-09-20','Khushal ',0,'8888111923'),(11,'papmdpgwap123@gmail.com',2535353,'2022-09-12','Khaushal',0,'8888111923'),(12,'papmdpgwap123@gmail.com',57575744,'2022-09-20','gjgjg',0,'8888111923'),(13,'papmdpgwap123@gmail.com',464646464,'2020-06-02','kartik bagade',0,'8888111923'),(14,'om@gmail.com',498998989,'2018-01-01','om',0,'1223456'),(15,'anil@gmail.com',35546566,'2019-06-03','anil',0,'654654611'),(16,'papmdpgwap123@gmail.com',45452464,'2021-06-01','cbfgh',0,'8888111923'),(17,'hsh@gmail.com',65662356,'2022-09-13','hari',0,'649696');
/*!40000 ALTER TABLE `passenger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `price` (
  `priceId` int NOT NULL AUTO_INCREMENT,
  `businessClassPrice` double NOT NULL,
  `economyClassPrice` double NOT NULL,
  `firstClassPrice` double NOT NULL,
  `flightId` int DEFAULT NULL,
  `routeId` int DEFAULT NULL,
  PRIMARY KEY (`priceId`),
  KEY `FKrhjsp3ayt1jenrspdk245fo81` (`flightId`),
  KEY `FKomt4mqoxvrbif8ucuwwo84y2m` (`routeId`),
  CONSTRAINT `FKomt4mqoxvrbif8ucuwwo84y2m` FOREIGN KEY (`routeId`) REFERENCES `route` (`routeId`),
  CONSTRAINT `FKrhjsp3ayt1jenrspdk245fo81` FOREIGN KEY (`flightId`) REFERENCES `flight` (`flightId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
INSERT INTO `price` VALUES (3,2000,1500,500,1,4),(9,2000,1500,1000,6,8),(10,5000,3000,2000,6,8),(11,5000,3000,2000,6,8),(12,5000,3000,2000,6,8);
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `route` (
  `routeId` int NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`routeId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` VALUES (4,'delhi','bengu'),(8,'Delhi','Pune');
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduleflight`
--

DROP TABLE IF EXISTS `scheduleflight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `scheduleflight` (
  `scheduleId` int NOT NULL AUTO_INCREMENT,
  `availableBusinessClassSeat` int DEFAULT NULL,
  `availableEconomyClassSeat` int DEFAULT NULL,
  `availableFirstClassSeat` int DEFAULT NULL,
  `landingDate` date DEFAULT NULL,
  `landingTime` time DEFAULT NULL,
  `takeoffDate` date DEFAULT NULL,
  `takeoffTime` time DEFAULT NULL,
  `flightId` int DEFAULT NULL,
  `routeId` int DEFAULT NULL,
  PRIMARY KEY (`scheduleId`),
  KEY `FKswyxfsi3wgorotav64ehf61uy` (`flightId`),
  KEY `FKnaxy5nh0qhfobwbkyuautnqsx` (`routeId`),
  CONSTRAINT `FKnaxy5nh0qhfobwbkyuautnqsx` FOREIGN KEY (`routeId`) REFERENCES `route` (`routeId`),
  CONSTRAINT `FKswyxfsi3wgorotav64ehf61uy` FOREIGN KEY (`flightId`) REFERENCES `flight` (`flightId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduleflight`
--

LOCK TABLES `scheduleflight` WRITE;
/*!40000 ALTER TABLE `scheduleflight` DISABLE KEYS */;
INSERT INTO `scheduleflight` VALUES (2,50,50,50,'2024-08-09','03:30:00','2024-04-09','20:10:00',1,4),(9,20,20,20,'2022-09-19','19:42:00','2022-09-18','17:42:00',6,8);
/*!40000 ALTER TABLE `scheduleflight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userId` int NOT NULL AUTO_INCREMENT,
  `dateOfBirth` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'1998-12-15','payal@gmail.com','payal','meshram','payal123','admin'),(6,'1997-05-04','khush@gmail.com','kushal','khairnar','45682','user'),(7,'1997-05-04','param123@gmail.com','param','nagre','123','user'),(15,NULL,'ak@gmail.com','akshay','kkon','ak123','user'),(16,'1997-04-05','mayur@gmail.com','Mayur','mistari','123456','user');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-23 23:48:08
